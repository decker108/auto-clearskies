# auto-clearskies

A BT-connected polling client and visualization website for the Airthings Wave Plus, built for
the RPi 4.

## Setup

1. sudo apt install libglib2.0-dev libpq-dev
2. venv auto-sunrise && cd auto-sunrise
3. bin/pip install wheel
4. Test the BT comms with: sudo bin/python read_waveplus.py <SERIAL_NUMBER>
5. bin/pip install -r requirements.txt

## External HDD setup

1. sudo mkdir /mnt/usb0
2. sudo mount -t vfat /dev/sda5 /mnt/usb0 -o umask=000
3. PARTUUID=39d6640e-05 /mnt/usb0 
4. UUID=AA66-5687 /mnt/usb0 vfat defaults,auto,users,rw,nofail,umask=000 0 0

## Export sqlite db to file

    sqlite3 /mnt/usb0/auto-clearskies/database.db
    sqlite> .headers on
    sqlite> .mode csv
    sqlite> .output data_export.csv
    sqlite> SELECT humidity,radon_st_avg,radon_lt_avg,temperature,pressure,co2_level,voc_level,created FROM measurements ORDER BY created DESC;
    sqlite> .quit

## Crontab setup

    */15 * * * * /bin/sh /home/pi/auto-clearskies/run_cronjob.sh

### Additional resources

* https://github.com/Airthings/waveplus-reader (python 2.x)
* https://github.com/kogant/waveplus-reader (python 3.x)
* https://airthings.com/tech/find_wave.py
