
CREATE TABLE public.measurements (
    id serial primary key NOT NULL,
    humidity float,
    radon_st_avg integer,
    radon_lt_avg integer,
    temperature float,
    pressure integer,
    co2_level integer,
    voc_level integer,
    created timestamp with time zone DEFAULT now()
);
