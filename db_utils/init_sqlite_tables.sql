CREATE TABLE IF NOT EXISTS measurements(
humidity,
radon_st_avg,
radon_lt_avg,
temperature,
pressure,
co2_level,
voc_level,
created DEFAULT CURRENT_TIMESTAMP);