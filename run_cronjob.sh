#! /bin/sh
set -e
export WAVEPLUS_SERIAL_NO=123456789 # replace with real serial no
export AUTO_CLEARSKIES_DB_FILE_PATH=/dev/null # replace with real db file path
export AUTO_CLEARSKIES_DB_HOST=localhost # replace with DO hostname
export AUTO_CLEARSKIES_DB_PORT=5432 # replace with actual pg port
export AUTO_CLEARSKIES_DB_USER=example # replace with dedicated user
export AUTO_CLEARSKIES_DB_PASS=example # replace with dedicated user password
echo "WAVEPLUS_SERIAL_NO=$WAVEPLUS_SERIAL_NO"
echo "AUTO_CLEARSKIES_DB_FILE_PATH=$AUTO_CLEARSKIES_DB_FILE_PATH"
/home/pi/auto-clearskies/bin/python /home/pi/auto-clearskies/src/cronjob.py
