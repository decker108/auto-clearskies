# MIT License
#
# Copyright (c) 2018 Airthings AS
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# https://airthings.com

from __future__ import print_function
from bluepy.btle import UUID, Peripheral, Scanner, DefaultDelegate
import struct
from device_errors import DeviceNotFoundError


class WavePlus:

    def __init__(self, SerialNumber):
        self.periph = None
        self.curr_val_char = None
        self.MacAddr = None
        self.SN = SerialNumber
        self.uuid = UUID("b42e2a68-ade7-11e4-89d3-123b93f75cba")
        logger.debug('Ctor done, SN: ' + str(self.SN))

    def connect(self):
        # Auto-discover device on first connection
        if self.MacAddr is None:
            scanner = Scanner().withDelegate(DefaultDelegate())
            searchCount = 0
            while self.MacAddr is None and searchCount < 50:
                devices = scanner.scan(0.1)  # 0.1 seconds scan period
                searchCount += 1
                for dev in devices:
                    logger.debug('Device: {0} {1} {2} {3} {4}'.format(
                        str(dev.addr), str(dev.iface), str(dev.addrType), str(dev.rssi), str(dev.connectable)))
                    ManuData = dev.getValueText(255)
                    logger.debug('Found device with raw serial number: ' + str(ManuData))
                    SN = self.parseSerialNumber(ManuData)
                    if SN == self.SN:
                        self.MacAddr = dev.addr  # exits the while loop on next conditional check
                        break  # exit for loop

            if self.MacAddr is None:
                logger.error("""ERROR: Could not find device.
                GUIDE: (1) Please verify the serial number.
                       (2) Ensure that the device is advertising.
                       (3) Retry connection.""".lstrip())
                raise DeviceNotFoundError('Could not find the Waveplus device')

        # Connect to device
        if self.periph is None:
            self.periph = Peripheral(self.MacAddr)
        if self.curr_val_char is None:
            self.curr_val_char = self.periph.getCharacteristics(uuid=self.uuid)[0]

    def read(self):
        if self.curr_val_char is None:
            logger.error("ERROR: Devices are not connected.")
            raise RuntimeError('Devices are not connected')
        rawdata = struct.unpack('<BBBBHHHHHHHH', self.curr_val_char.read())
        return Sensors(rawdata)

    def disconnect(self):
        if self.periph is not None:
            self.periph.disconnect()
            self.periph = None
            self.curr_val_char = None

    @staticmethod
    def parseSerialNumber(ManuDataHexStr):
        if ManuDataHexStr == "None" or ManuDataHexStr is None:
            return "Unknown"
        else:
            ManuData = bytearray.fromhex(ManuDataHexStr)

            if ((ManuData[1] << 8) | ManuData[0]) == 0x0334:
                SN = ManuData[2]
                SN |= (ManuData[3] << 8)
                SN |= (ManuData[4] << 16)
                SN |= (ManuData[5] << 24)
            else:
                SN = "Unknown"
            return SN

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, type, value, traceback):
        self.disconnect()


class Sensors:
    def __init__(self, rawData):
        sensor_version = rawData[0]
        if sensor_version == 1:
            self.humidity = rawData[1] / 2.0
            self.radon_short_term_avg = self.convertToRadon(rawData[4])
            self.radon_long_term_avg = self.convertToRadon(rawData[5])
            self.temperature = rawData[6] / 100.0
            self.rel_atm_pressure = rawData[7] / 50.0
            self.co2 = rawData[8] * 1.0
            self.voc = rawData[9] * 1.0
        else:
            logger.error("""ERROR: Unknown sensor version.
                   GUIDE: Contact Airthings for support.""".lstrip())
            raise RuntimeError('Unknown sensor version')

    @staticmethod
    def convertToRadon(radon_raw):
        # Either invalid measurement, or not available
        if 0 <= radon_raw <= 16383:
            return radon_raw
        return -1


def getSensorData(_logger, SerialNumberEnvVar):
    global logger
    logger = _logger
    with WavePlus(SerialNumberEnvVar) as waveplus:
        try:
            # print "Device serial number: %s" % SerialNumber

            # read values
            sensorReadings = waveplus.read()

            # extract
            humidity = sensorReadings.humidity
            radon_st_avg = sensorReadings.radon_short_term_avg
            radon_lt_avg = sensorReadings.radon_long_term_avg
            temperature = sensorReadings.temperature
            pressure = sensorReadings.rel_atm_pressure
            CO2_lvl = sensorReadings.co2
            VOC_lvl = sensorReadings.voc

            header = ['Humidity', 'Radon ST avg', 'Radon LT avg', 'Temperature', 'Pressure', 'CO2 level', 'VOC level']
            data = [humidity, radon_st_avg, radon_lt_avg, temperature, pressure, CO2_lvl, VOC_lvl]

            return {k: v for k, v in zip(header, data)}
        except Exception as e:
            logger.exception(e)
            raise e
