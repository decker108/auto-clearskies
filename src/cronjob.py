from airthings_client import getSensorData
from measurements_repository import insertMeasurements
from logger_handler import setupLogging
import sys
import os
from time import sleep
from device_errors import DeviceNotFoundError
import postgres_repository as repo
from bluepy.btle import BTLEDisconnectError


def getSensorDataWithRetries(retries=5, serialNo=0):
    for _ in range(retries):
        try:
            return getSensorData(log, serialNo)
        except (DeviceNotFoundError, BTLEDisconnectError) as e:
            log.debug("Error getting sensor data (error: %s, message: %s), retrying in 0.5s", str(e.__class__.__name__), e.message if hasattr(e, 'message') else "")
            sleep(0.5)
            pass
    raise RuntimeError("Failed to get sensor readings after {} tries".format(retries))


if __name__ == '__main__':
    log = setupLogging()
    DATABASE_HOST = os.environ.get('AUTO_CLEARSKIES_DB_HOST', 'localhost')
    DATABASE_PORT = os.environ.get('AUTO_CLEARSKIES_DB_PORT', '5432')
    DATABASE_NAME = os.environ.get('AUTO_CLEARSKIES_DB_NAME', 'autoclearskiesdb')
    DATABASE_USER = os.environ.get('AUTO_CLEARSKIES_DB_USER', 'postgres')
    DATABASE_PASS = os.environ.get('AUTO_CLEARSKIES_DB_PASS', 'test')
    use_sqlite = os.environ.get("AUTO_CLEARSKIES_DB_FILE_PATH", '/dev/null') != '/dev/null'
    try:
        log.debug('Started background job')
        data = getSensorDataWithRetries(5, int(os.environ['WAVEPLUS_SERIAL_NO']))
        if use_sqlite:
            insertMeasurements(data)
        repo.insertMeasurements(data, DATABASE_HOST, DATABASE_PORT, DATABASE_NAME, DATABASE_USER, DATABASE_PASS)
        log.debug('Completed background job')
    except:
        log.error('Error running background job', exc_info=True)
        sys.exit(-1)
