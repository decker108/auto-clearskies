class DeviceNotFoundError(Exception):
    """ Unspecified run-time error. """
    def __init__(self, message): # real signature unknown
        #super(DeviceNotFoundError, self).__init__(message)
        self.message = message

    def __str__(self):
        return self.message
