import logging
from logging.handlers import RotatingFileHandler
from os import environ


def setupLogging():
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    logger = logging.getLogger("auto-clearskies")
    logger.setLevel(logging.INFO)

    logfilePath = environ.get("AUTO_CLEARSKIES_LOG_PATH", "/var/log/auto_clearskies/app.log")
    rfh = RotatingFileHandler(logfilePath, maxBytes=1000000, backupCount=2)
    rfh.setFormatter(formatter)

    strh = logging.StreamHandler()
    strh.setFormatter(formatter)

    logger.addHandler(rfh)
    # logger.addHandler(strh)
    return logger
