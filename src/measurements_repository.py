import sqlite3
from os import environ

DATABASE_NAME = environ['AUTO_CLEARSKIES_DB_FILE_PATH']

columns = ['humidity', 'radon_st_avg', 'radon_lt_avg', 'temperature', 'pressure', 'co2_level', 'voc_level', 'created']


def insertMeasurements(data):
    keys = ['Humidity', 'Radon ST avg', 'Radon LT avg', 'Temperature', 'Pressure', 'CO2 level', 'VOC level']
    with sqlite3.connect(DATABASE_NAME, 5) as dbConn:
        dbConn.row_factory = sqlite3.Row
        dbConn.execute("""INSERT INTO measurements(humidity, radon_st_avg, radon_lt_avg, temperature, pressure, 
                       co2_level, voc_level, created) 
                       VALUES(?, ?, ?, ?, ?, ?, ?, datetime("now"))""".lstrip(), [data[key] for key in keys])


def getMeasurements(fromTimestamp=0):
    with sqlite3.connect(DATABASE_NAME, 5) as dbConn:
        dbConn.row_factory = sqlite3.Row
        cursor = dbConn.cursor()
        cursor.execute('''SELECT humidity, radon_st_avg, radon_lt_avg, temperature, pressure, co2_level, voc_level, 
            created 
        FROM measurements 
        WHERE created >= ?''', [fromTimestamp])
        rows = cursor.fetchmany()
        output = []
        for row in rows:
            output.append({k: v for k, v in zip(columns, row)})
        return output


def getLatestMeasurements():
    with sqlite3.connect(DATABASE_NAME, 5) as dbConn:
        dbConn.row_factory = sqlite3.Row
        cursor = dbConn.cursor()
        cursor.execute('''SELECT humidity, radon_st_avg, radon_lt_avg, temperature, pressure, co2_level, voc_level, 
            created 
        FROM measurements 
        ORDER BY rowid DESC 
        LIMIT 1''')
        row = cursor.fetchone()
        return {k: v for k, v in zip(columns, row)}
