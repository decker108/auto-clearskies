import psycopg2


def insertMeasurements(data, dbHost, dbPort, dbName, dbUser, dbPass):
    connectionString = f"host={dbHost} port={dbPort} dbname={dbName} user={dbUser} password={dbPass}"
    with psycopg2.connect(connectionString) as conn:
        with conn.cursor() as cur:
            # keys = ['Humidity', 'Radon ST avg', 'Radon LT avg', 'Temperature', 'Pressure', 'CO2 level', 'VOC level']
            cur.execute("""INSERT INTO measurements(humidity, radon_st_avg, radon_lt_avg, temperature, pressure, 
                       co2_level, voc_level, created) 
                       VALUES(%s, %s, %s, %s, %s, %s, %s, now())""".lstrip(),
                        (data['Humidity'], data['Radon ST avg'], data['Radon LT avg'], data['Temperature'],
                         data['Pressure'], data['CO2 level'], data['VOC level']))
