from datetime import datetime, timedelta
from flask import Flask, render_template, make_response
import json
import measurements_repository as repo
from logger_handler import setupLogging

app = Flask(__name__)
sensorUnits = {
    'voc_level': 'ppb',
    'temperature': 'degC',
    'pressure': 'hPa',
    'radon_lt_avg': 'Bq/m3',
    'co2_level': 'ppm',
    'humidity': '%rH',
    'radon_st_avg': 'Bq/m3'
}
log = setupLogging()


@app.route('/')
def index():
    measurements = repo.getLatestMeasurements()
    context = {'measurements': measurements, 'sensorUnits': sensorUnits}
    response = make_response(render_template('index.html', **context), 200)
    response.headers['Cache-Control'] = 'no-store'
    return response


@app.route('/compact')
def compactIndex():
    measurements = repo.getLatestMeasurements()
    context = {'measurements': measurements, 'sensorUnits': sensorUnits}
    response = make_response(render_template('compact_index.html', **context), 200)
    response.headers['Cache-Control'] = 'no-store'
    return response


@app.route('/measurements/all')
@app.route('/measurements/since/<int:fromTimestamp>')
def getMeasurements(fromTimestamp=0):
    measurements = repo.getMeasurements(fromTimestamp)
    return _create_json_response(measurements)


@app.route('/measurements/latest')
def getLatestMeasurements():
    measurements = repo.getLatestMeasurements()
    return _create_json_response(measurements)


@app.route('/measurements/lastweek')
def getMeasurementsSinceLastWeek():
    fromTimestamp = int((datetime.today() - timedelta(days=7)).timestamp())
    measurements = repo.getMeasurements(fromTimestamp)
    return _create_json_response(measurements)


@app.route('/measurements/lastmonth')
def getMeasurementsSinceLastMonth():
    fromTimestamp = int((datetime.today() - timedelta(days=30)).timestamp())
    measurements = repo.getMeasurements(fromTimestamp)
    return _create_json_response(measurements)


@app.route('/measurements/lastyear')
def getMeasurementsSinceLastYear():
    fromTimestamp = int((datetime.today() - timedelta(days=365)).timestamp())
    measurements = repo.getMeasurements(fromTimestamp)
    return _create_json_response(measurements)


def _create_json_response(measurements):
    response = make_response(json.dumps(measurements), 200)
    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-store'
    return response
